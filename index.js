const express = require("express"); // Tương tự : import express from "express";
const path = require("path");

// Khởi tạo Express App
const app = express();

//Khai báo port sử dụng
const port = 8000;

// Khai báo APi dạng Get "/" sẽ chạy vào đây
app.get("/", (request, response) => {
    console.log(__dirname);
    //Chạy file HTML với đường dẫn / cần dòng 2
    response.sendFile(path.join(__dirname + "/views/index.html"));

})

//Để hiển thị ảnh cần thêm middleware static vào express
app.use(express.static(__dirname + "/views"))


app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})